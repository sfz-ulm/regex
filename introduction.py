#!/bin/python3

import re

print('Enter regular expression:')
regex = input()

while True:
  print()
  print('Enter string:')
  text = input()
  match = re.match(regex, text)
  if match == None:
    print('No match!')
  else:
    print('Match!')
    print('Groups: %s' % str(match.groups()))

